# GorryWell Backend Test

Hi, welcome to the test interview

In this test you have to create User Access Level module or we usually call it user role. You have to define dynamic user level up to 3 level or can flexible if we want to add another level

In our case we have 3 roles like this:
#- Mentor (lv.1)
#-- Mentee (lv.2)
#--- Normal User (lv.3)

And also we have 1 page for each type of user. 
1. Mentor Page (only access by mentor) can see list of mentee owned by mentor
2. Classroom Page (can be access by mentee and mentor) can see list of exercise available for the mentee and mentor can manage the exercise
4. Leaderboard ( can access by all type of user but required to login 1st) the page can see leaderboard and report for each classroom

## The TASK
What you have to do to complete the objective, you need to create the API for login, mentor page, classroom page, leaderboard page. Lets assume all the data already insert by someone so you don't need to put more detail like classroom creation or classroom deletation, you only need to create data visualization function with specific user restriction. 

Of course in this case Login function is one of the key maker to do success with your objective, you have to put more attention on this like encryption method or everything you need to do for security enhanching just do it.

## Requirement
You have to followup this requirement or development stack
1. Nodejs with expressJs framework
2. Database with mongoDb (mongoose)
3. There is no restriction for additional module usage, it's all depends on your application design

## Scoring System
In order to pass the test so you can go to the next interview session or final interview, you have to deliver a good result and you have to follow this criteria:
1. Code quality, make sure you did clean code
2. Have consistency in the code you write
3. Make meaningful name class, function, and variable
4. Your creativity
5. Api design

You have 1 week to complete this task, please make your own repo is up to you to use Github, Gitlab, Bitbucket. I won't accept .zip file
